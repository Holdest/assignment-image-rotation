#include "image_arch.h"
#include "berror.h"

#include <stdio.h>
#include <unistd.h>

static bool is_file_exists( const char* path ) { return access( path, F_OK ) == 0; }

extern const char* bmp_read_msgs[];
extern const char* bmp_write_msgs[];

static struct berror do_main( int argc, char** argv ) {
    if ( argc != 3 )
        return berror_of( EC_NOT_ENOUGH_ARGUMENTS, "" );

    const char* fin_name  = argv[ 1 ];
    const char* fout_name = argv[ 2 ];  

    if ( !is_file_exists( fin_name ) )
        return berror_of( EC_FILE_NOT_FOUND, fin_name );

    FILE* in = fopen( fin_name, "rb" );
    if ( in == NULL ) return berror_of( EC_CANNOT_OPEN_FILE, fin_name );

    struct image source = {0};
    
    enum read_status in_status = from_bmp( in, &source );
    if ( fclose( in ) != 0 ) return berror_of( EC_CANNOT_CLOSE_FILE, fin_name );
    if ( in_status != READ_OK ) return berror_of( EC_CANNOT_READ_FILE, bmp_read_msgs[ in_status ] );

    struct image dest = rotate( source );
    image_destroy( source );

    FILE* out = fopen( fout_name, "wb" );
    if ( out == NULL ) return berror_of( EC_CANNOT_OPEN_FILE, fout_name );

    enum write_status out_status = to_bmp( out, &dest );
    image_destroy( dest );
    if ( fclose( out ) != 0 ) return berror_of( EC_CANNOT_CLOSE_FILE, fout_name );
    if ( out_status == WRITE_ERROR ) return berror_of( EC_CANNOT_WRITE_FILE, bmp_write_msgs[ out_status ] );

    return berror_of( EC_OK, "" );
}

int main( int argc, char** argv ) {
    struct berror err = do_main( argc, argv );
    err.handler( err );
    return err.code;
}
