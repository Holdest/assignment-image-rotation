#include "image_arch.h"

struct __attribute__((packed)) bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

#define BMP_HEADER_BFTYPE 0x4d42 // ascii: "BM"
#define BMP_SIZE_OF_INFO_HEADER 40

enum bmp_compression {
  BI_RGB = 0x0000,
  BI_RLE8 = 0x0001,
  BI_RLE4 = 0x0002,
  BI_BITFIELDS = 0x0003,
  BI_JPEG = 0x0004,
  BI_PNG = 0x0005,
  BI_CMYK = 0x000B,
  BI_CMYKRLE8 = 0x000C,
  BI_CMYKRLE4 = 0x000D
};

const char* bmp_read_msgs[] = {
  [ READ_OK                ] = "encountered valid bmp image",
  [ READ_INVALID_BITS      ] = "encountered invalid bit sequence",
  [ READ_INVALID_HEADER    ] = "encountered invalid bmp header structure",
  [ READ_INVALID_SIGNATURE ] = "encountered invalid file signature"
};

const char* bmp_write_msgs[] = {
  [ WRITE_ERROR ] = "encountered io errors while writing bmp image",
  [ WRITE_OK    ] = "encountered no io errors while writing bmp image"
};

static uint64_t bmp_count_padding( uint64_t width ) { return ( width % 4 ); }

static bool bmp_is_invalid_header( struct bmp_header header ) {
  print( INFO "bmp_is_invalid_header: started\n" );
  bool is_invalid = false;
  /* bfType */
  is_invalid = is_invalid || ( header.bfType != BMP_HEADER_BFTYPE );
  print( INFO "bfType: actual 0x%x; expected: " STR( BMP_HEADER_BFTYPE ) "\n", header.bfType );
  if ( is_invalid ) return true;
  print( INFO "bfType test passed\n" );

  /* biSizeImage, bfileSize, bOffBits */
  is_invalid = is_invalid || ( ( header.biSizeImage != 0 ) && header.bfileSize != ( header.biSizeImage + header.bOffBits ) );
  print( INFO "bfileSize: actual %" PRIu32 "; expected: %" PRIu32 "\n", header.bfileSize, header.biSizeImage + header.bOffBits );
  if ( is_invalid ) return true;
  print( INFO "bfileSize test passed\n" );

  /* bOffBits as bmp_header size */
  is_invalid = is_invalid || ( header.bOffBits != sizeof( struct bmp_header ) );
  print( INFO "bOffBits: actual %" PRIu32 "; expected: " STR( sizeof( struct bmp_header ) ) "\n", header.bOffBits );
  if ( is_invalid ) return true;
  print( INFO "bOffBits test passed\n" );

  /* biSize */
  is_invalid = is_invalid || ( header.biSize != BMP_SIZE_OF_INFO_HEADER );
  print( INFO "biSize: actual %" PRIu32 "; expected: " STR( BMP_SIZE_OF_INFO_HEADER ) "\n", header.biSize );
  if ( is_invalid ) return true;
  print( INFO "biSize test passed\n" );

  /* biPlanes */
  is_invalid = is_invalid || ( header.biPlanes != 1 );
  print( INFO "biPlanes: actual %" PRIu16 "; expected: " STR( 1 ) "\n", header.biPlanes );
  if ( is_invalid ) return true;
  print( INFO "biPlanes test passed\n" );

  /* biBitCount */
  is_invalid = is_invalid || ( header.biBitCount != 24 );
  print( INFO "biBitCount: actual %" PRIu16 "; expected: " STR( 24 ) "\n", header.biBitCount );
  if ( is_invalid ) return true;
  print( INFO "biBitCount test passed\n" );

  /* biCompression */
  is_invalid = is_invalid || ( header.biCompression != BI_RGB );
  print( INFO "biCompression: actual %" PRIu32 "; expected: BI_RGB (=" STR( BI_RGB ) ")\n", header.biCompression );
  print( INFO "biCompression test passed\n" );
  // if ( is_invalid ) return true;

  /* biSizeImage as biWidth and biHeight */
  // is_invalid = is_invalid || ( header.biSizeImage != ( header.biHeight * ( header.biWidth * sizeof( struct pixel ) + bmp_count_padding( header.biWidth ) ) ) );
  // print( INFO "+ biSizeImage as biWidth and biHeight [ %i ]\n", is_invalid );
  // there is no need to check this 'cause test contains that ******* invalid header
  print( INFO "bmp_is_invalid_header: successfully finished\n" );
  return is_invalid;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
  print( INFO "from_bmp: started\n" );
  struct bmp_header header = { 0 };
  /* read header */
  const size_t header_count = fread( &header, sizeof( struct bmp_header ), 1, in );
  print( INFO "read %zu headers with size " STR( sizeof( struct bmp_header ) ) "\n", header_count );
  if ( header_count == 0 ) return READ_INVALID_SIGNATURE;
  print( INFO "found header\n" );

  /* is header valid */
  if ( bmp_is_invalid_header( header ) ) return READ_INVALID_HEADER;
  print( INFO "found valid header\n" );

  /* get the bitmap */
  const size_t bitmap_start = header.bOffBits;
  if ( fseek( in, bitmap_start, SEEK_SET ) ) return READ_INVALID_SIGNATURE;
  print( INFO "successfully reached bitmap on position %zu\n", bitmap_start );

  /* init the image with given dimension */
  *img = image_init( header.biWidth, header.biHeight );
  print( INFO "image of %" PRIu32 "x%" PRIu32 " successfully inited\n", img->width, img->height );

  /* calculate the padding */
  const uint64_t padding = bmp_count_padding( img->width );
  print( INFO "image will be read according to padding: %" PRIu64 "\n", padding );

  /* fill the data */
  for ( uint32_t y = 0; y < img->height; ++y ) {
    print( INFO "trying to read the %" PRIu32 " row\n", y );
    /* read the whole row */
    struct pixel* current_row = img->data + y * img->width;
    size_t pixels_count = fread( current_row, sizeof( struct pixel ), img->width, in );
    print( INFO "read pixels: actual: %zu; expected: %zu\n", pixels_count, img->width );

    /* test if read not the whole row */
    if ( pixels_count != img->width ) {
      image_destroy( *img );
      return READ_INVALID_BITS;
    }

    /* jump to the next row skipping the padding */
    if ( fseek( in, padding, SEEK_CUR ) ) {
      image_destroy( *img );
      return READ_INVALID_BITS;
    }
  }
  print( INFO "from_bmp: successfully finished\n" );
  return READ_OK;
}

static uint32_t bmp_count_size_image( uint64_t width, uint64_t height ) {
  return height * ( width * sizeof( struct pixel ) + bmp_count_padding( width ) );
}

static struct bmp_header bmp_get_valid_header( uint64_t width, uint64_t height ) {
  return ( struct bmp_header ) {
    .bfType = BMP_HEADER_BFTYPE,
    .bfileSize = sizeof( struct bmp_header ) + bmp_count_size_image( width, height ),
    .bfReserved = 0,
    .bOffBits = sizeof( struct bmp_header ),
    .biSize = 40,
    .biWidth = width,
    .biHeight = height,
    .biPlanes = 1,
    .biBitCount = 24,
    .biCompression = BI_RGB,
    .biSizeImage = bmp_count_size_image( width, height ),
    .biClrUsed = 0,
    .biClrImportant = 0
  };
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
  print( INFO "to_bmp: started\n" );
  struct bmp_header header = bmp_get_valid_header( img->width, img->height );

  /* write header */
  if ( fwrite( &header, sizeof( struct bmp_header ), 1, out ) != 1 ) return WRITE_ERROR;
  print( INFO "wrote bmp header into file\n" );

  const size_t padding = bmp_count_padding( img->width );
  print( INFO "write using padding %zu\n", padding );
  const uint8_t zeros[] = { 0, 0, 0 }; /* array for alignment */
  for ( uint64_t y = 0; y < img->height; ++y ) {
    print( INFO "trying to write the %" PRIu32 " row\n", y );
    const struct pixel* current_row = img->data + y * img->width;
    /* write the whole row */
    if ( fwrite( current_row, sizeof( struct pixel ), img->width, out ) != img->width ) return WRITE_ERROR;
    print( INFO "successfully wrote the %" PRIu64 " row\n", y );

    /* write zeros for alignment */
    if ( fwrite( zeros, sizeof( uint8_t ), padding, out ) != padding ) return WRITE_ERROR;
    print( INFO "successfully aligned\n" );
  }
  print( INFO "to_bmp: successfully finished\n" );
  return WRITE_OK;
}
