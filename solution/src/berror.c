#include "berror.h"

static void handle_not_enough_args( struct berror self ) {
  (void)self; // to suppress warning and save the invocation interface
  print( ALERT "not enough arguments passed\n" );
  print( USAGE "./image-transformer <source-image> <transformed-image>\n" );
}

static void handle_not_found( struct berror self ) {
  print( ALERT "file \"%s\" doesn't exist; check if the correct path was provided\n", self.reason );
}

#define CANNOT_FILE_OP( op ) "cannot " op " file: %s\n"

static void handle_cannot_open( struct berror self ) {
  print( ALERT CANNOT_FILE_OP( "open" ), self.reason );
}

static void handle_cannot_close( struct berror self ) {
  print( ALERT CANNOT_FILE_OP( "close" ), self.reason );
}

static void handle_cannot_read( struct berror self ) {
  print( ALERT CANNOT_FILE_OP( "read" ), self.reason );
}

static void handle_cannot_write( struct berror self ) {
  print( ALERT CANNOT_FILE_OP( "write" ), self.reason );
}

#undef CANNOT_FILE_OP

static void handle_ok( struct berror self ) {
  (void)self; // to suppress warning and save the invocation interface
  print( INFO "successfully exited\n" );
} 

const struct berror errors[] = {
  [ EC_NOT_ENOUGH_ARGUMENTS ] = { .code =    DEFAULT_ERROR_CODE, .reason = "", .handler = handle_not_enough_args },
  [ EC_FILE_NOT_FOUND       ] = { .code =    DEFAULT_ERROR_CODE, .reason = "", .handler = handle_not_found },
  [ EC_CANNOT_OPEN_FILE     ] = { .code =    DEFAULT_ERROR_CODE, .reason = "", .handler = handle_cannot_open },
  [ EC_CANNOT_CLOSE_FILE    ] = { .code =    DEFAULT_ERROR_CODE, .reason = "", .handler = handle_cannot_close },
  [ EC_CANNOT_READ_FILE     ] = { .code =    DEFAULT_ERROR_CODE, .reason = "", .handler = handle_cannot_read },
  [ EC_CANNOT_WRITE_FILE    ] = { .code =    DEFAULT_ERROR_CODE, .reason = "", .handler = handle_cannot_write },
  [ EC_OK                   ] = { .code = DEFAULT_NO_ERROR_CODE, .reason = "", .handler = handle_ok }
};

struct berror berror_of( enum error_codes ec, const char* reason ) {
  struct berror err = errors[ ec ];
  err.reason = reason;
  return err;
}
