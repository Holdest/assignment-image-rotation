#include "image.h"

struct image image_init( const uint64_t width, const uint64_t height ) {
  struct pixel* data = ( struct pixel* ) malloc( sizeof( struct pixel ) * width * height );
  return ( struct image ) { .width = width, .height = height, .data = data };
}

void image_destroy( struct image image ) { free( image.data ); }

static uint64_t image_get_pixel_address( uint64_t w, uint64_t x, uint64_t y ) { return y * w + x; }

void image_set_pixel( struct image dest, uint64_t addr, const struct pixel pixel ) { dest.data[ addr ] = pixel; }

struct pixel image_get_pixel( struct image source, uint64_t addr ) { return source.data[ addr ]; }

struct image rotate( struct image const source ) {
  struct image dest = image_init( source.height, source.width );
  for ( uint64_t y = 0; y < dest.height; ++y ) {
    const uint64_t sx = y;
    for ( uint64_t x = 0; x < dest.width; ++x ) {
      const uint64_t sy = source.height - ( x + 1 );
      image_set_pixel( dest,
        image_get_pixel_address( dest.width, x, y ),
        image_get_pixel( source, image_get_pixel_address( source.width, sx, sy ) ) );
    }
  }
  return dest;
}
