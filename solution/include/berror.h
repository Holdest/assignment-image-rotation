#ifndef _BERROR_H_
#define _BERROR_H_

#include <stdint.h>

#include "util.h"

#define DEFAULT_ERROR_CODE (-1)
#define DEFAULT_NO_ERROR_CODE (0)

enum error_codes {
    EC_NOT_ENOUGH_ARGUMENTS,
    EC_FILE_NOT_FOUND,
    EC_CANNOT_OPEN_FILE,
    EC_CANNOT_CLOSE_FILE,
    EC_CANNOT_READ_FILE,
    EC_CANNOT_WRITE_FILE,
    EC_OK
};

struct berror;

typedef void (handler)( struct berror self );

struct berror {
    handler* handler;
    uint32_t code;
    const char* reason;
};

struct berror berror_of( enum error_codes ec, const char* reason );

#endif /* _BERROR_H_ */
