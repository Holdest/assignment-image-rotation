#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdint.h>
#include <malloc.h>

#include "pixel.h"

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image image_init( const uint64_t width, const uint64_t height );

void image_destroy( struct image image );

void image_set_pixel( struct image dest, uint64_t addr, const struct pixel pixel );

struct pixel image_get_pixel( struct image source, uint64_t addr );

struct image rotate( struct image const source );

#endif /* _IMAGE_H_ */
