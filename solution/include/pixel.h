#ifndef _PIXEL_H_
#define _PIXEL_H_

#include <stdint.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

#endif /* _PIXEL_H_ */
