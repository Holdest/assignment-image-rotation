#ifndef _IMAGE_ARCH_H_
#define _IMAGE_ARCH_H_

#include  <stdio.h>
#include  <stdint.h>
#include  <stdbool.h>
#include  <inttypes.h>

#include  "image.h"
#include  "util.h"


/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
  /* коды других ошибок  */
  };

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );


#endif /* _IMAGE_ARCH_H_ */
