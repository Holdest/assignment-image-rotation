#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define STRINGIFY( x ) #x
#define STR( x ) STRINGIFY( x )

#define _DEF_LOG_LEVEL( name, colour ) colour #name ": " ANSI_COLOR_RESET

#define INFO  _DEF_LOG_LEVEL( info,  ANSI_COLOR_YELLOW )
#define USAGE _DEF_LOG_LEVEL( usage, ANSI_COLOR_GREEN  )
#define ALERT _DEF_LOG_LEVEL( alert, ANSI_COLOR_RED    )

void print( const char* format, ... );

#endif /* _UTIL_H_ */
